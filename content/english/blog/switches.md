---
title: "The lego mechanical keyboard"
date: 2021-02-20T12:52:36+00:00
author: ame
image_webp: images/blog/m65-tealio.webp
image: images/blog/m65_tealio.jpg
description : "gateron turquoise tealio 65g"
---
so seems lately I was lucky at raffles... after a kam set few days after i got a set, came the time of switches....

thought I found the perfect linear kailh&novelkey new cream https://www.aliexpress.com/item/1005002197276054.html

or my kailh crystal jades with click bar removed....

till by chance i see the kprepublic raffle on gateron turquoise tealio

https://www.aliexpress.com/item/1005002991068601.html

opted for the 65g...

are they good? yes

are they cheap? no, but if you do not buy switches every week probably is ok

shall I lube? yes

shall I film? I would say yes but this is up to your pleasure.

I also have a good collection of "home made" switches in general spirit springs.. gateron ink v2 yellows and reds...

now my switches go in general on these keyboards... https://gitlab.com/drFaustroll/m65 so I appreciate my tastes are not in line with the majority... I tried my best to record them how they sound stock... in hand and on a board with and without caps (caps are mt3 godspeed if you wonder)

sounds in here https://imgur.com/a/552vzcg
