---
title: "Curriculum vitæ"
---


### Work Experience


- Science and Technology Facilities Council, Daresbury Laboratory\
  July 2015 **Computational Scientist**
- Irish Centre for High-End Computing, Dublin Offices \
  July 2013 -- June 2015 **Computational Scientist**
- Irish Centre for High-End Computing, Dublin Offices \
  February 2009 -- August 2010 **Computational Scientist**


### Education

-   School of Physics, University College Dublin \
    September 2010 -- July 2013, PhD student,\
    supervisors Professor Giovanni Ciccotti and Dr Simone Meloni
-   School of Mathematics and Physics, Atomistic Simulation Centre, Queen's University Belfast \
    August 2004 -- January 2009   PhD student,   Time Dependent Tight-Binding+U: Applications to fluorescent molecules (never submitted)\
    supervisors Professor Tony Paxton and Dr Tchavdar Todorov
-   Faculty of Physics, University of Bucharest \
    October 200 - June 2004 , BSc., 9.85/10, Honours, English taught lectures department\
    Diploma Thesis:   Molecular Orbitals Quantum Calculations for Structures Based on Si, C, N -- Vibrational Analysis , supervisor
    Reader Mircea Bercu
-   High School "Gib Mihaescu" , Dragasani, Informatics class\
    September 1996 - June 2000 , GPA 9.87/10


### Teaching Experience

-   2014-2015, PH504 Graduate Module, Introduction to Parallel Programming@NUIG
-   2014, GM training course, Introduction to HPC, Software Installation, Benchmarking, Parallel Programming components
-   2014 Introduction to Parallel Programming
-   2013-2014, PH504 Graduate Module, Introduction to Parallel Programming@NUIG
-   2012 -2013 Tutorials: Computational Biophysics and Nanoscale Simulations @University College Dublin
-   2011 -2012   Microlabs   Introduction to C++ programming @University College Dublin\
-   2010 -2011   Microlabs   Introduction to C++ programming  
    @University College Dublin
-   2009-2010   Courses   Introduction to MPI and OpenMP ,  
    Introduction to Modern Fortran @different universities across
    Ireland
-   2007-2008   Tutorials   Professional Skills Programme (C/C++
    Programming)   Vector Algebra and Dynamics ,   Waves and Vector
    Fields   @Queen's University Belfast
-   2006-2007   Tutorials   Professional Skills Programme (C/C++
    Programming)   Vector Algebra and Dynamics ,   Computational Methods
    in Physics ,   Computational Modelling in Physics , Waves and Vector
    Fields   @Queen's University Belfast
-   2005-2006   Lecture   Professional Skills Programme (C/C++
    Programming)   -- second half, Tutorials:   Vector Algebra and
    Dynamics ,   Computational Methods in Physics ,   Computational
    Modelling in Physics ,   Waves and Vector Fields   @Queen's
    University Belfast
-   2004-2005   Tutorials:   Vector Algebra and Dynamics ,  
    Computational Methods in Physics , Computational Modelling in
    Physics ,   Waves and Vector Fields   @Queen's University Belfast

### Training

-   21-25   June 2010 ,   Basic techniques and tools for development and
    maintenance of atomic-scale software , CECAM, Zaragoza, Spain
-   14-16   April 2009 ,   Intel Cluster Software Development Tools   ,
    SGI, Reading, UK
-   January 2007 ,   Programming SMP Computers , HPCx, Belfast, UK
-   December 2006 , Winter School,   Electronic excitations and
    spectroscopies : Theory and Codes , CECAM, Lyon, France
-   July 2006 , Summer School,   UK Grad Programme , University of
    Exeter, UK
-   January 2006 , Winter School,   Programming Parallel Computers ,   ,
    CECAM, Julich, Germany
-   October-November 2005 , Autumn School,   Understanding Molecular
    Simulations ,   , CECAM, Amsterdam, Holland
-   August-September 2004 , Summer School,   Time Dependent Density
    Functional Theory: Applications and Prospects ,   Benasque, Spain
-   August-September 2003 , Research Stage,   High resolution
    fluorescent laser spectroscopy , at Joint Institute for Nuclear
    Research, Dubna, Moscow Region, Russia
-   June 2003 , Intensive Lectures,   Simulation and characterization
    spectrometric methods of physical processes for micro and
    nanotechnologies , at   INCDM Bucharest, MINAMAT-NET
-   August-September 2002 , Research Stage,   High resolution
    fluorescent laser spectroscopy , at Joint Institute for Nuclear
    Research, Dubna, Moscow Region, Russia

### Prizes

-   2005   -- Musgrave Demonstrators' Prize
-   2004   -- Best Academic Results by   Bucovineni Sarghie Brothers'
    Foundation   and   University of Bucharest
-   2004   -- Honourous Mention,   CyberEducation Contest , organised by
      University of Bucharest , Project   Didactical Molecular Dynamics

### Skills

-   Programming:   Fortran 90&95, C/C++, on different platforms,
    parallel (MPI and OpenMP)\
    libraries and serial versions; scientific programming for atomic and
    molecular physics
-   Project Building:   cross-OS experience with CMake -- since 2006 I
    have used\
    CMake to build and port different scientific applications on
    Linux/MacOSX/Windows platforms\
    ( [TDTB+UJ](http://tdtbuj.googlecode.com) ,  
    [ATEN](http://www.projectaten.org) )
-   Web:   HTML, CSS, php+Mysql -- September 2004 -- September 2007
    webmaster of the Atomistic Simulation Centre  
    <http://titus.phy.qub.ac.uk>
-   Hardware   troubleshooting solving skills
-   Installation and maintenance   of different operating systems from
    Microsoft, Unix/Linux like


### Languages

|                |                                               |
| --------------:|:----------------------------------------------|
|**English**     |    spoken   good/fluent   written   good      |
|**French**      |    above beginner                             |
|**Spanish**     |    beginner                                   |
|**Romanian**    |    mother tongue                              |


### Membership

-   Institute of Physics
-   OpenSUSE
-   Collège de 'Pataphysique


### Hobbies

-   philosophy, music, urban culture and architecture, contemporary and
    absurd literature

last updated 2015

to be updated 01/03/2020
